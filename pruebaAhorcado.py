'''
Created on 12/10/2020

@author: rafael
'''
from array import array
from _io import open
from random import randint
import os.path as path
class ManipulacionArchivo:
    def archivoVacio(self):
        ruta='palabras.txt'
        palabrasArchivo=''
        if(not path.exists(ruta)):#if que previene que exista el archivo
            print("Oh vaya no hay archivo de texto no te preocupes lo creare y de una vez llenalo :)")
            self.LenarArchivo()
        archivo=open(ruta,"r")
        palabrasArchivo=archivo.read()
        archivo.close()
        if(palabrasArchivo==''):
            return True
        else:
            return False
    def verificarArchivo(self):
        ruta='palabras.txt'
        palabrasArchivo=""
        if(not path.exists(ruta)):
            print("Oh vaya no hay archivo de texto no te preocupes lo creare y de una vez llenalo :)")
            self.LenarArchivo()
        archivo=open(ruta,"r")
        palabrasArchivo=archivo.read()
        archivo.close()
        numComas=0
        poci=0
        for poci in range(poci,len(palabrasArchivo)):
            if(palabrasArchivo[poci]==","):
                numComas=numComas+1
        if(numComas>0):
            print(f"Numero de palabras es: {(numComas)}")
        elif(len(palabrasArchivo)==0):
            print("El archivo esta vacio")
        elif (numComas==0):
            print(f"Numero de palabras es: {(numComas+2)}")
                
    def LenarArchivo(self):
        ruta='palabras.txt'
        print("La forma para llenar el archivo sera de la siguiente forma:")
        print("PERRO,GATO,AMARILLO (Lo mismo para ingles)")
        palabrsaEsp=input("Ingresa las palabras en español con el formato dicho con anterioridad: ").upper()
        palabrasIng=input("Ingresa las palabras en ingles con el formto anterior: ")
        v1=self.ordenamientoMezclaDirecta(palabrsaEsp.split(","))
        v2=self.ordenamientoMezclaDirecta(palabrasIng.split(","))
        separador=0
        palabras=""
        while len(v1)>=1:
            palabras=palabras+v1.pop(0)+","
        palabras=palabras+"/"
        while len(v2)>=1:
            palabras=palabras+v2.pop(0)+","
        c=0
        i=0
        print(palabras)
        for i in range(i,len(palabras)):
            if(palabras[i]=="/"):
                c=c+1
        if(c==1):
            archivo=open(ruta,"w")
            archivo.write(palabras)
            archivo.close()
        else:
            print("Oh vaya alparecer no ingresaste el separador")
            archivo.close()
        
    def borrarArchivo(self):
        import os
        ruta='palabras.txt'
        if(os.path.exists(ruta)):
            os.remove(ruta)
            creacion=open(ruta,'w')
            creacion.close()
            print("Se borro el archivo y se creo uno en blanco")
        else:
            print("Oh vaya no existe el archivo que quieres borrar pero tranquilo creare uno nuevo para ti :)")
            creacion=open(ruta,'w')
            creacion.close()
    def obtenerPalabrasArchivo(self):
        if(self.archivoVacio()==False):
            ruta='palabras.txt'
            palabrasArchivo=""
            archivo=open(ruta,"r")
            palabrasArchivo=archivo.read()
            archivo.close()
            return palabrasArchivo
        else:
            return '!'
    def hangman(self):
        if(self.archivoVacio()):
            return True
        else:
            return False
    
    def ordenamientoMezclaDirecta(self,array):
        
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()
            
            self.ordenamientoMezclaDirecta(arregloIz)
            
            self.ordenamientoMezclaDirecta(arregloDer)
            
            #Hasta aqui es la divicion de todos los elemntos hasta que llege a ser igual a 1
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):# si la pocicion de la izquierda es menor a la derecha
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
            #Hace que siempre se este actualizando ya que se elimina la pocicion
            
            #Ahora esto es por si llegan a quedar elementos sobrantes
            while len(arregloIz)>0:
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))
        
        return array     
        

class Vidas:
    def __init__(self):
        self.vidas=10
    def quitarVida(self):
        self.vidas=self.vidas-1
#----------------------------------------------------------------

class JuegoAhorcado (Vidas,ManipulacionArchivo):
    
    def cargarPalabras(self,listaPalabras):
        palabras=listaPalabras.split('/')
        return palabras
    def elegirPalabra(self,palabrasCar):
        separacion=palabrasCar.split(('/'))
        palabrasESP=separacion[0]
        palabrasING=separacion[1]
        opEleccion=""
        palabraJuegoMasIdioma=[]
        while(not (opEleccion=="1" or opEleccion=="2")):
            print("1- Jugar con una palabra en español")
            print("2- Jugar Con una palabra en ingles")
            opEleccion=input()
            if(opEleccion=="1"):
                print("Elegiste jugar en español")
                palabrasESPSep=palabrasESP.split(",")
                #print(palabrasESPSep)
                '''
                for x in range(0,len(palabrasESPSep)-1):
                    print(x,"--> ",palabrasESPSep[x])
                '''
                #el -2 es para evitar que se elija una palabra vacia 
                pociAleatoria=randint(0,len(palabrasESPSep)-2)
                palabraJuegoMasIdioma.append(palabrasESPSep[pociAleatoria])
                palabraJuegoMasIdioma.append("1")
            elif(opEleccion=="2"):
                print("Elegiste jugar en Ingles")
                palabrasIngSep=palabrasING.split(",")
                #print(palabrasIngSep)
                pociAleatoria=randint(0,len(palabrasIngSep)-2)
                palabraJuegoMasIdioma.append(palabrasIngSep[pociAleatoria])
                palabraJuegoMasIdioma.append("2")
            else:
                print("Por favor elige una opcion Disponible")
        return palabraJuegoMasIdioma
    
    def ordenLetarsingresadas(self,letras):#ordena letras ingresadas
        cad1=""
        i=0
        for i in range(0,len(letras)):
            cad1=cad1+letras[i]+","
            i=i+1
        vector=cad1.split(",")
        vector2=self.ordenamientoMezclaDirecta(vector)
        return vector2
    
    def busquedaBinaria(self,array,letra):
        inicio=0
        final=len(array)-1
        
        while inicio<= final:
            puntero=(inicio+final)//2
            if letra==array[puntero]:
                return 1
            elif letra > array[puntero]:
                inicio=puntero+1
            else:
                final=puntero-1
        return 0
    
    def inicioAhorcado(self,palabraIdioma):
        ganador=False
        palabraSecret=palabraIdioma[0]
        idioma=palabraIdioma[1]
        oportunidades=Vidas()
        letrasIngresadas=TDALetrasIngresadas()
        copia=[]
        if(len(palabraSecret)>0):
            print("Palabra secreta: ",palabraSecret.lower())
            print(f"Estoy pensando en una palabra con {len(palabraSecret)} letras")
        else:
            print("Llenaste incorrectamente el archivo seras enviado al menu principal pero antes llenaras el archivo")
            self.LenarArchivo()
        palabra=""
        while((ganador==False and oportunidades.vidas>0) and len(palabraSecret)>0):
            contador=0
            copia=[]
            for i in letrasIngresadas.pilaLetras:
                copia.append(ord(i))
            
            print(f"Te quedan {oportunidades.vidas} oportunidades")
            print(f"Letras disponibles: {self.obtenerLetrasDisponibles(copia)}")
            letraIngresada=input("Ingresa una letra: ").lower()
            letraIngresada.lower()
            copia=[]
            arrayLetras=[]
            #print(f"'{letraIngresada}'")
            if len(letraIngresada)==0:
                print("Entro al if")
                letraIngresada="1"
            for i in letrasIngresadas.pilaLetras:
                copia.append(ord(i))
                
            letrasDis=self.obtenerLetrasDisponibles(copia)
            existencia=0
            #--------------------------Aqui va el algoritmo de busqueda-------------------
            v1=self.ordenLetarsingresadas(letrasDis)
            existencia=self.busquedaBinaria(v1,letraIngresada)
            #----------------------
            if(existencia>0):
                letrasIngresadas.insertarLetra(letraIngresada)
                
                for x in range(0,len(palabraSecret)):
                    if(letraIngresada==palabraSecret[x].lower()):
                        contador=contador+1
                if(contador==0):
                    print("Oh vaya la letra que ingresaste no estaba en la palabra secreta")
                    oportunidades.quitarVida()
                    copia=[]
                    for i in letrasIngresadas.pilaLetras:
                        copia.append(ord(i))
                else:
                    copia=[]
                    for i in letrasIngresadas.pilaLetras:
                        copia.append(ord(i))
                palabra=self.obtenerPalabraAdivinada(palabraSecret,copia)
                print(palabra)
            else:
                if(letraIngresada.isdigit() or (not (letraIngresada.isalpha()))):
                    print("Oh vaya alparecer no ingresaste una letra si no algun otro caracter")
                elif(len(letraIngresada)>1):
                    print("Ingresaste mas de un caracter")
                else:
                    print("Oh vaya la letra que ingresaste ya la habias ingresado con anterioridad")
            if(self.seAdivinoPalabra(palabra,letrasIngresadas)):
                ganador=True
            else:
                ganador=False
            print("---------------------------------------------------------------------")
        if(ganador==True):
            print("Felicidades ganaste adivinaste la palabra secreta :)")
        elif(oportunidades.vidas==0):
            print(f"Oh vaya te quedaste sin vidas disponibles la palabra secreta era '{palabraSecret}'") 
                   
    def seAdivinoPalabra(self,palabraSecreta,letrasIngresadas):
        
        letras=letrasIngresadas.pilaLetras
        palabraSecreta=palabraSecreta.replace(" ","")
        letras=self.ordenLetarsingresadas(letras)
        tamaño=len(palabraSecreta)
        num=0
        if len(palabraSecreta)>0:
            for i in range(0,len(palabraSecreta)):
                pocicion=palabraSecreta[i]
                for x in range(0,len(letras)):
                    if pocicion==letras[x]:
                        num=num+1
        else:
            return False
        #print(num)
        #print(tamaño)
        return num==tamaño
    def obtenerPalabraAdivinada(self,palabra,letrasIngresadas):
        carac="!#$%&/()=?�'<>-_.:,;}{~+*"
        palabraCodificada=[]
       
        for a in range(0,len(palabra)):
            palabraCodificada.append(carac[a])
        #---------------------------------------
        i=0
        for i in range(0,len(letrasIngresadas)):
            for x in range(0,len(palabra)):
                if(chr(letrasIngresadas[i])==palabra[x].lower()):
                    poci=palabraCodificada[x]
                    palabraCodificada[x]=chr(letrasIngresadas[i])
            
        cad=""
        for z in range(0,len(palabraCodificada)):
            if(ord(palabraCodificada[z])>=97 and ord(palabraCodificada[z])<=122):
                cad=cad+palabraCodificada[z]+" "
            else:
                cad=cad+"-"+" "
        return cad
        
    def obtenerLetrasDisponibles(self,letrasIngresadas):
        abecedario='abcdefghijklmnñopqrstuvwxyz'
        abcAsc=[]
        for i in range(0,len(abecedario)):
            abcAsc.append(ord(abecedario[i]))
        #print(abcAsc)
        #print(letrasIngresadas)
        #Metodo de busqueda Secuencial
        if(len(letrasIngresadas)>0):
            for x in range(0,len(abcAsc)):
                pocicion=abcAsc[x]
                for y in range(0,len(letrasIngresadas)):
                    if(pocicion==letrasIngresadas[y]):
                        #print(f"Pocicion {pocicion} --> {letrasIngresadas[y]}")
                        abcAsc[x]=32
        
        abecedario=''
        for z in range(0,len(abcAsc)):
            if(abcAsc[z]==32):
                abecedario=abecedario+""
            else:
                abecedario=abecedario+chr(abcAsc[z])
                       
        return abecedario



#------------------------------------------------------Pruebas--------------------

class TDALetrasIngresadas:
    def __init__(self):
        self.pilaLetras=[]
        self.cima=-1
    
    def insertarLetra(self,letra):
        self.cima=self.cima+1
        self.pilaLetras.append(letra)
    def obtenerLetras(self):
        return self.pilaLetras.copy()


op=""
archivo=ManipulacionArchivo()
while(op!="5"):
    print("Elige la opcion que deseas")
    print("1) Verificar Archivo")
    print("2) Llenar Archivo con palabras")
    print("3) Borrar archivo")
    print("4) Jugar")
    print("5) Salir")
    op=input()
    if(op=="1"):
        archivo.verificarArchivo()
    elif(op=="2"):
        archivo.LenarArchivo()
    elif(op=="3"):
        archivo.borrarArchivo()
    elif(op=="4"):
        if(not archivo.hangman()):
            juego=JuegoAhorcado()
            palabrasCargadas=[]
            palabrasCargadas=archivo.obtenerPalabrasArchivo()
            if(not palabrasCargadas=='!'):
                juego.inicioAhorcado(juego.elegirPalabra(palabrasCargadas))
                
        else:
            print("Oh vaya el archivo esta Vacio")
            
        
    elif(op=="5"):
        print("Saliendo.....")
    else:
        print("La opcion que seleccionaste no existe prueba otra opcion")

